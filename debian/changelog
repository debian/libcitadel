libcitadel (917-5) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Use secure URI in debian/watch.
  * Use secure URI in Homepage field.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 27 Nov 2022 21:32:13 +0000

libcitadel (917-4) unstable; urgency=medium

  * QA upload.
  * Convert to 3.0 (quilt) format (Closes: #1007403).
  * Use debhelper-compat level 12 (11 is deprecated).

 -- Bastian Germann <bage@debian.org>  Thu, 25 Aug 2022 13:43:52 +0200

libcitadel (917-3) unstable; urgency=medium

  * Orphaning package
  * Point VCS information to salsa.

 -- Michael Meskes <meskes@debian.org>  Thu, 11 Jul 2019 19:28:32 +0200

libcitadel (917-2) unstable; urgency=medium

  * Re-added rules target that was lost in the last version. (Closes: #886434)

 -- Michael Meskes <meskes@debian.org>  Sat, 06 Jan 2018 09:54:15 +0100

libcitadel (917-1) unstable; urgency=medium

  * Updated watch file to new manifest.
  * New upstream version 917
  * Added hardening options.
  * Added symbols file.

 -- Michael Meskes <meskes@debian.org>  Fri, 05 Jan 2018 13:53:54 +0100

libcitadel (916-1) unstable; urgency=medium

  * New upstream version 916
  * Bumped Standards-Version to 4.1.3, no changes needed.
  * Switched priority from extra to optional.
  * Switch to latest debhelper version.
  * Removed rules stanza for binary independent packages.
  * Made package multiarch aware.

 -- Michael Meskes <meskes@debian.org>  Tue, 02 Jan 2018 10:53:59 +0100

libcitadel (904-1) unstable; urgency=medium

  * New upstream version 904

 -- Michael Meskes <meskes@debian.org>  Sat, 03 Jun 2017 08:40:59 +0200

libcitadel (902-1) unstable; urgency=medium

  * Imported Upstream version 902
  * Bumped debhelper and standards version, no changes needed.
  * Make sure libcitadel.pc has a valid version number.

 -- Michael Meskes <meskes@debian.org>  Fri, 13 May 2016 10:35:58 +0200

libcitadel (9.01-1) unstable; urgency=medium

  * Imported Upstream version 9.01
  * Bumped Standards-Version to 3.9.6, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Thu, 18 Jun 2015 12:15:50 +0200

libcitadel (8.24-1) unstable; urgency=medium

  * Imported Upstream version 8.24
  * Bumped Standards-Version to 3.9.5, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Tue, 28 Jan 2014 09:54:15 +0100

libcitadel (8.22-1) unstable; urgency=low

  * Imported Upstream version 8.22
  * Update config.* files during build. (Closes: #727399)

 -- Michael Meskes <meskes@debian.org>  Mon, 04 Nov 2013 13:15:34 +0100

libcitadel (8.20-1) unstable; urgency=low

  * Imported Upstream version 8.20
  * Changed package name according to soname.

 -- Michael Meskes <meskes@debian.org>  Sat, 24 Aug 2013 11:28:03 +0200

libcitadel (8.16-1) unstable; urgency=low

  * Imported Upstream version 8.16
  * Bumped Standards-Version to 3.9.4, no changes needed.
  * Added hardening build flags.

 -- Michael Meskes <meskes@debian.org>  Sun, 10 Mar 2013 11:38:23 +0100

libcitadel (8.14-2) unstable; urgency=low

  [ Wilfried Goesgens ]
  * Add upstream patch to null terminate string

  [ Michael Meskes ]
  * Updated configure.in from upstream

 -- Wilfried Goesgens <dothebart@citadel.org>  Tue, 27 Nov 2012 12:15:38 +0100

libcitadel (8.14-1) unstable; urgency=low

  * Imported Upstream version 8.14

 -- Michael Meskes <meskes@debian.org>  Fri, 20 Jul 2012 20:38:04 +0200

libcitadel (8.12-1) unstable; urgency=low

  * Imported Upstream version 8.12

 -- Michael Meskes <meskes@debian.org>  Thu, 28 Jun 2012 10:40:18 +0200

libcitadel (8.11-2) unstable; urgency=low

  * Applied upstream's patch to fix compilation problem on kFreeBSD.
    (Closes: #675841)

 -- Michael Meskes <meskes@debian.org>  Tue, 05 Jun 2012 11:30:24 +0200

libcitadel (8.11-1) unstable; urgency=low

  * Adjusted watch file for new url.
  * Imported Upstream version 8.11

 -- Michael Meskes <meskes@debian.org>  Fri, 25 May 2012 11:28:54 +0200

libcitadel (8.05-1) unstable; urgency=low

  * Imported Upstream version 8.05
  * Bumped Standards-Version to 3.9.3, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Sat, 17 Mar 2012 20:13:04 +0100

libcitadel (8.04-1) unstable; urgency=low

  * New upstream version
  * Added build-arch and build-indep targets to rules file.
  * Removed {...} braces from debhelper file.

 -- Michael Meskes <meskes@debian.org>  Thu, 12 Jan 2012 12:31:46 +0100

libcitadel (7.86-1) unstable; urgency=low

  * Imported Upstream version 7.86
  * Bumped Standards-Version to 3.9.2, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Sun, 24 Apr 2011 21:54:36 +0200

libcitadel (7.84-1) unstable; urgency=low

  * New Upstream version 7.84

 -- Michael Meskes <meskes@debian.org>  Fri, 10 Sep 2010 11:34:30 +0200

libcitadel (7.83-1) unstable; urgency=low

  * New Upstream version 7.83
  * Bumped Standards-Version to 3.9.1, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Tue, 17 Aug 2010 09:54:13 +0200

libcitadel (7.81-1) unstable; urgency=low

  * New Upstream version 7.81
  * Bumped Standards-Version to 3.9.0, no changes needed.
  * Added source/format file.
  * Updated copyright file.

 -- Michael Meskes <meskes@debian.org>  Wed, 21 Jul 2010 09:16:12 +0200

libcitadel (7.72-2) unstable; urgency=low

  * Added upstream patch to fix bug in RFC822 decoder.
  * Removed patch system with empty patches directory.

 -- Michael Meskes <meskes@debian.org>  Mon, 22 Feb 2010 09:55:21 +0100

libcitadel (7.72-1) unstable; urgency=low

  * New Upstream version.
  * Bumped Standards-Version to 3.8.4, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Sat, 20 Feb 2010 17:59:33 +0100

libcitadel (7.71-1) unstable; urgency=high

  * New Upstream version.

 -- Michael Meskes <meskes@debian.org>  Wed, 06 Jan 2010 10:57:02 +0100

libcitadel (7.70-1) unstable; urgency=low

  * New Upstream version.

 -- Michael Meskes <meskes@debian.org>  Fri, 18 Dec 2009 09:29:06 +0100

libcitadel (7.66-1) unstable; urgency=low

  * New upstream version.

 -- Michael Meskes <meskes@debian.org>  Tue, 29 Sep 2009 12:14:04 +0200

libcitadel (7.63-1) unstable; urgency=low

  * New upstream version.

 -- Michael Meskes <meskes@debian.org>  Sat, 05 Sep 2009 14:19:50 +0200

libcitadel (7.61-1) unstable; urgency=low

  * New upstream version.

 -- Michael Meskes <meskes@debian.org>  Wed, 12 Aug 2009 12:56:51 +0200

libcitadel (7.60-1) unstable; urgency=low

  * New upstream version.
  * Bumped Standards-Version to 3.8.2, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Wed, 05 Aug 2009 14:19:13 +0200

libcitadel (7.51-1) unstable; urgency=low

  * New upstream version.
  * Updated watch file for new page layout.
  * Added missing Homepage: field.

 -- Michael Meskes <meskes@debian.org>  Thu, 04 Jun 2009 09:01:02 +0200

libcitadel (7.50-1) unstable; urgency=low

  * New upstream version.

 -- Michael Meskes <meskes@debian.org>  Thu, 09 Apr 2009 19:37:54 +0200

libcitadel (7.43-1) unstable; urgency=high

  * New upstream version.
  * Bumped Standards-Version to 3.8.1, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Wed, 18 Mar 2009 11:29:15 +0100

libcitadel (7.42-1) unstable; urgency=low

  * New upstream version.
  * Bumped major version to 2.
  * Fixing copyright notice, closes: #518489

 -- Michael Meskes <meskes@debian.org>  Wed, 11 Mar 2009 17:50:23 +0100

libcitadel (7.41-1) unstable; urgency=low

  * New upstream version, closes: #497984
  * Changed watch file to account for an additional letter in the version
    number if present.

 -- Michael Meskes <meskes@debian.org>  Fri, 20 Feb 2009 16:49:05 +0100

libcitadel (7.38a-1) unstable; urgency=low

  * New upstream version.
  * Removed upstream prepatches that were no longer needed.

 -- Michael Meskes <meskes@debian.org>  Mon, 22 Sep 2008 16:15:32 +0200

libcitadel (7.37-4) unstable; urgency=low

  [Wilfried Goesgens]
  *  [r6615] change vcard version to 2.1

 -- Michael Meskes <meskes@debian.org>  Tue, 23 Sep 2008 19:08:29 +0200

libcitadel (7.37-4) unstable; urgency=low

  [Wilfried Goesgens]
  * [r6580] add upstream prepatch to not violate vcard
    standard, closes: #497948

 -- Michael Meskes <meskes@debian.org>  Tue, 9 Sep 2008 19:08:29 +0200

libcitadel (7.37-3) unstable; urgency=low

  * Enable patching in debian/rules.
  * Re-diffed patches.

 -- Michael Meskes <meskes@debian.org>  Fri, 15 Aug 2008 13:08:29 +0200

libcitadel (7.37-2) unstable; urgency=low

  [Wilfried Goesgens]
  * add dependency on quilt
  * [r6413] added upstream prepatch; 64 bit cleanness
  * [r6475] added upstream prepatch; close dirhandle after use

 -- Michael Meskes <meskes@debian.org>  Wed, 13 Aug 2008 10:04:19 +0200

libcitadel (7.37-1) unstable; urgency=low

  * New upstream version.

 -- Michael Meskes <meskes@debian.org>  Fri, 20 Jun 2008 12:16:53 +0200

libcitadel (1.14-1) unstable; urgency=low

  * New upstream version
  * Bumped standards to 3.8.0.

 -- Michael Meskes <meskes@debian.org>  Fri, 06 Jun 2008 11:48:05 +0200

libcitadel (1.09-2) unstable; urgency=low

  * Made shlibs file list version number.

 -- Michael Meskes <meskes@debian.org>  Thu, 01 May 2008 15:22:46 +0200

libcitadel (1.09-1) unstable; urgency=low

  * New upstream version
  * Added watch file.

 -- Michael Meskes <meskes@debian.org>  Thu, 24 Apr 2008 09:51:49 +0200

libcitadel (1.08-1) unstable; urgency=low

  * Changed maintainer to Debian Citadel Team.
  * Moved priority to extra.
  * New upstream version.
  * Do not used deprecated substvar notion anymore.
  * Added debian/compat file.
  * Do not ignore errors in clean target.

 -- Michael Meskes <meskes@debian.org>  Sat, 22 Mar 2008 17:18:06 +0100

libcitadel (1.05-4) unstable; urgency=high

   * include xdgmime

 -- Wilfried Goesgens <w.goesgens@outgesourced.org>  Tue, 12 Feb 2008 0:00:00 +0001

libcitadel (1.03-3) unstable; urgency=low

    * initial debian release

 -- Wilfried Goesgens <w.goesgens@outgesourced.org>  Sun, 18 Nov 2007 23:55:21 +0100
